from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout
from django_celery_results.models import TaskResult
from django.core.paginator import Paginator
from django.contrib import messages
from django.http import JsonResponse
from celery.result import AsyncResult

def home(request):
    return render(request, 'index.html')

def tasks(request):
    tasks = TaskResult.objects.order_by('-id')
    paginator = Paginator(tasks, 25)

    page = request.GET.get('page')

    return render(request, 'tasks.html', {
        'tasks': paginator.get_page(page)
    })

def logout(request):
    messages.add_message(request, messages.SUCCESS, 'You logged out succesfully.')

    auth_logout(request)
    return redirect('/account/login/')

def task_status(request):
    task_id = request.GET.get('task_id')
    if task_id:
        async_result = AsyncResult(task_id)
        if async_result.ready():
            return JsonResponse({'finish': async_result.result})
    return JsonResponse({'finish': False})