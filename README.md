# Pleio control panel

This is the control panel for Pleio administrators to manage sites and copy them between different regions.

## Development

1. Start the control container

```
docker-compose up
```

2. Migrate the database

```
docker-compose exec control python manage.py migrate
```
3. Create a admin user

```
docker-compose exec control python manage.py createsuperuser
```

## Getting a shell inside the control container

If you need to troubleshoot your setup  locally you can get a shell inside the control container by running this command.

```
docker-compose exec control /bin/ash
```

## Building

To build a docker container of this project:

```
docker build -t pleio/control .
```

this builds a new docker image.