from sqlalchemy import Table, Column, Integer, BigInteger, String, Text, LargeBinary, MetaData

access_collection_membership = Table('elgg_access_collection_membership', MetaData(),
    Column('user_guid', BigInteger),
    Column('access_collection_id', BigInteger)
)

access_collections = Table('elgg_access_collections', MetaData(),
    Column('id', BigInteger),
    Column('name', Text),
    Column('owner_guid', BigInteger),
    Column('site_guid', BigInteger)
)

annotations = Table('elgg_annotations', MetaData(),
    Column('id', BigInteger),
    Column('entity_guid', BigInteger),
    Column('name_id', BigInteger),
    Column('value_id', BigInteger),
    Column('value_type', String),
    Column('owner_guid', BigInteger),
    Column('access_id', BigInteger),
    Column('time_created', BigInteger),
    Column('enabled', String)
)

backup = Table('elgg_backup', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('transaction_id', BigInteger),
    Column('site_guid', BigInteger),
    Column('time_created', BigInteger),
    Column('performed_by', BigInteger),
    Column('type', String),
    Column('data', LargeBinary)
)

config = Table('elgg_config', MetaData(),
    Column('name', String, primary_key=True),
    Column('value', Text),
    Column('site_guid', BigInteger)
)

datalists = Table('elgg_datalists', MetaData(),
    Column('name', String, primary_key=True),
    Column('value', Text)
)

entities = Table('elgg_entities', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('type', String),
    Column('subtype', Integer),
    Column('owner_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('container_guid', BigInteger),
    Column('access_id', BigInteger),
    Column('time_created', BigInteger),
    Column('time_updated', BigInteger),
    Column('last_action', BigInteger),
    Column('enabled', String)
)

entity_relationships = Table('elgg_entity_relationships', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('guid_one', BigInteger),
    Column('relationship', String),
    Column('guid_two', BigInteger),
    Column('time_created', BigInteger)
)

entity_subtypes = Table('elgg_entity_subtypes', MetaData(),
    Column('id', Integer, primary_key=True),
    Column('type', String),
    Column('subtype', String),
    Column('class', String)
)

entity_views = Table('elgg_entity_views', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('type', String),
    Column('subtype', Integer),
    Column('container_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('views', BigInteger)
)

entity_views_log = Table('elgg_entity_views_log', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('entity_guid', BigInteger),
    Column('type', String),
    Column('subtype', Integer),
    Column('container_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('performed_by_guid', BigInteger),
    Column('time_created', Integer)
)

groups_entity = Table('elgg_groups_entity', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('name', Text),
    Column('description', Text)
)

metadata = Table('elgg_metadata', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('entity_guid', BigInteger),
    Column('name_id', BigInteger),
    Column('value_id', BigInteger),
    Column('value_type', String),
    Column('owner_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('access_id', BigInteger),
    Column('time_created', BigInteger),
    Column('enabled', String)
)

metastrings = Table('elgg_metastrings', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('string', Text)
)

objects_entity = Table('elgg_objects_entity', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('title', Text),
    Column('description', Text)
)

private_settings = Table('elgg_private_settings', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('entity_guid', BigInteger),
    Column('name', String),
    Column('value', Text)
)

push_notifications = Table('elgg_push_notifications', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('user_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('container_guid', BigInteger),
    Column('count', BigInteger)
)

river = Table('elgg_river', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('type', String),
    Column('subtype', String),
    Column('action_type', String),
    Column('site_guid', BigInteger),
    Column('access_id', BigInteger),
    Column('view', Text),
    Column('subject_guid', BigInteger),
    Column('object_guid', BigInteger),
    Column('annotation_id', BigInteger),
    Column('posted', BigInteger)
)

sites_entity = Table('elgg_sites_entity', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('name', Text),
    Column('description', Text),
    Column('url', String)
)

system_log = Table('elgg_system_log', MetaData(),
    Column('id', BigInteger, primary_key=True),
    Column('object_id', BigInteger),
    Column('object_class', Text),
    Column('object_type', String),
    Column('object_subtype', String),
    Column('event', String),
    Column('performed_by_guid', BigInteger),
    Column('owner_guid', BigInteger),
    Column('site_guid', BigInteger),
    Column('access_id', BigInteger),
    Column('enabled', String),
    Column('time_created', BigInteger),
    Column('ip_address', String),
)

users_entity_without_pleio_guid = Table('elgg_users_entity', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('name', String),
    Column('username', String),
    Column('password', String),
    Column('salt', String),
    Column('password_hash', String),
    Column('email', String),
    Column('language', String),
    Column('code', String),
    Column('banned', String),
    Column('admin', String),
    Column('last_action', BigInteger),
    Column('prev_last_action', BigInteger),
    Column('last_login', BigInteger),
    Column('prev_last_login', BigInteger)
)

users_entity_with_pleio_guid = Table('elgg_users_entity', MetaData(),
    Column('guid', BigInteger, primary_key=True),
    Column('name', String),
    Column('username', String),
    Column('password', String),
    Column('salt', String),
    Column('password_hash', String),
    Column('email', String),
    Column('language', String),
    Column('code', String),
    Column('banned', String),
    Column('admin', String),
    Column('last_action', BigInteger),
    Column('prev_last_action', BigInteger),
    Column('last_login', BigInteger),
    Column('prev_last_login', BigInteger),
    Column('pleio_guid', BigInteger)
)