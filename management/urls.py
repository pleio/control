from django.urls import path
from . import views

app_name = 'management'

urlpatterns = [
    path('<str:backend_key>/', views.overview, name='overview'),
    path('<str:backend_key>/copy/<int:site_id>/', views.copy, name='copy'),
    path('<str:backend_key>/edit/<int:site_id>/', views.edit, name='edit'),
    path('<str:backend_key>/delete/<int:site_id>/', views.delete, name='delete'),
    path('', views.overview, name='overview'),
    path('<str:backend_key>/admin_email_addresses', views.admin_email_addresses, name='admin_email_addresses')
]
