import os
import sys
from django.core.management.base import BaseCommand, CommandError
from management.lib import Manager

class Command(BaseCommand):
    help = "Disable instance"
    
    def __init__(self, *args, **kwargs):
        pass

    def add_arguments(self, parser):
        parser.add_argument('--key', help='instance key', required=True)
        parser.add_argument('--host', help='update hostname', type=str)
        parser.add_argument('--enabled', help='update enabled', type=int)

    def handle(self, *args, **options):
        key = options.get("key")
        enabled = options.get("enabled", None)
        host = options.get("host", None)

        manager = Manager.Instance()

        backend = manager.backends[os.getenv('TRANSFER_BACKENDS_NAME')]

        # check if site already exists
        if not backend.key_exists(key):
            sys.stdout.write(f"instance with {key} does not exists\n")
            sys.exit(1)

        if host != None:
            connection = backend.get_connection()
            connection.execute("UPDATE instances SET host = %s WHERE username = %s", [host, key])
            connection.close()
            sys.stdout.write(f"updated {key} host to {host}\n")

        if enabled != None:
            if enabled > 0:
                enabled = 1
            connection = backend.get_connection()
            connection.execute("UPDATE instances SET enabled = %s WHERE username = %s", [enabled, key])
            connection.close()
            sys.stdout.write(f"updated {key} enabled to {enabled}\n")
