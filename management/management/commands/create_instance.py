import os
import sys
from django.core.management.base import BaseCommand, CommandError
from management.lib import Manager

class Command(BaseCommand):
    help = "Create new instance database"
    
    def __init__(self, *args, **kwargs):
        pass

    def add_arguments(self, parser):
        parser.add_argument('--key', help='site key', required=True)
        parser.add_argument('--host', help='site hostname', required=True)
        parser.add_argument('--prod', help='environment', default=False, action="store_true")


    def handle(self, *args, **options):
        instance_key = options.get("key")
        instance_host = options.get("host")
        instance_is_prod = options.get("prod")

        if len(instance_key) < 2:
            sys.stdout.write('Error: key should be longer then 2 characters\n')
            sys.exit(1)

        if instance_key in ['root', 'pleio-control', 'mysql']:
            sys.stdout.write('key in blocked list\n')
            sys.exit(1)            

        manager = Manager.Instance()

        backend = manager.backends[os.getenv('TRANSFER_BACKENDS_NAME')]

        # check if site already exists
        if backend.key_exists(instance_key):
            sys.stdout.write(f"Site with key {instance_key} allready exists\n")
            sys.exit(1)           

        # check if host already exists
        if backend.host_exists(instance_host):
            sys.stdout.write(f"Site with host {instance_host} allready exists\n")
            sys.exit(1)         

        try :
            site = backend.create_site({
                'id': 0,
                'key': instance_key,
                'host': instance_host,
                'enabled': 0,
                'env': "prod" if instance_is_prod else "test"
            })
        except Exception as e:
            sys.stdout.write(f"Error creating instance with key {instance_key}. Message: {e}")
            sys.exit(1)    

        sys.stdout.write(f"Succesfully created instance {instance_key}\n")
