from __future__ import absolute_import, unicode_literals
import os
import subprocess
import threading
import queue
from time import time
from shutil import copyfile
from django.utils.crypto import get_random_string
from celery import shared_task
from .lib import Manager
from . import elgg_models
from management.backends.pleio_legacy.base import BackendWrapper
from pleio_control import settings

@shared_task(bind=True)
def copy_site(self, source_backend_key, source_site_id, destination_backend_key, destination_key, destination_host):
    meta = {
        'type': 'copy_site',
        'destination_host': destination_host,
        'progress': 0
    }

    self.update_state(state='PROGRESS', meta=meta)

    manager = Manager.Instance()

    source_backend = manager.backends[source_backend_key]
    destination_backend = manager.backends[destination_backend_key]

    source_site = source_backend.get_site(source_site_id)
    destination_site = destination_backend.create_site({
        'id': 0,
        'key': destination_key,
        'host': destination_host,
        'enabled': True,
        'env': 'test'
    })

    source_site.initialize_export()

    # access_collections
    rows = source_site.bulk_read(elgg_models.access_collections)
    destination_site.bulk_write(elgg_models.access_collections, rows)

    # access_collection_membership
    rows = source_site.bulk_read(elgg_models.access_collection_membership)
    destination_site.bulk_write(elgg_models.access_collection_membership, rows)

    # annotations
    rows = source_site.bulk_read(elgg_models.annotations)
    destination_site.bulk_write(elgg_models.annotations, rows)

    # entities
    rows = source_site.bulk_read(elgg_models.entities)
    destination_site.bulk_write(elgg_models.entities, rows)

    # entity_relationships
    rows = source_site.bulk_read(elgg_models.entity_relationships)
    destination_site.bulk_write(elgg_models.entity_relationships, rows)

    meta['progress'] = 10
    self.update_state(state='PROGRESS', meta=meta)

    # entity_subtypes
    rows = source_site.bulk_read(elgg_models.entity_subtypes)
    destination_site.bulk_write(elgg_models.entity_subtypes, rows)

    # objects_entity
    rows = source_site.bulk_read(elgg_models.objects_entity)
    destination_site.bulk_write(elgg_models.objects_entity, rows)

    # groups_entity
    rows = source_site.bulk_read(elgg_models.groups_entity)
    destination_site.bulk_write(elgg_models.groups_entity, rows)

    # sites_entity
    rows = source_site.bulk_read(elgg_models.sites_entity)
    destination_site.bulk_write(elgg_models.sites_entity, rows)

    # entity_views
    rows = source_site.bulk_read(elgg_models.entity_views)
    destination_site.bulk_write(elgg_models.entity_views, rows)

    meta['progress'] = 20
    self.update_state(state='PROGRESS', meta=meta)

    # entity_views_log
    rows = source_site.bulk_read(elgg_models.entity_views_log)
    destination_site.bulk_write(elgg_models.entity_views_log, rows)

    # metadata
    rows = source_site.bulk_read(elgg_models.metadata)
    destination_site.bulk_write(elgg_models.metadata, rows)

    meta['progress'] = 30
    self.update_state(state='PROGRESS', meta=meta)

    # metastrings
    rows = source_site.bulk_read(elgg_models.metastrings)
    destination_site.bulk_write(elgg_models.metastrings, rows)

    # private_settings
    rows = source_site.bulk_read(elgg_models.private_settings)
    destination_site.bulk_write(elgg_models.private_settings, rows)

    # river
    rows = source_site.bulk_read(elgg_models.river)
    destination_site.bulk_write(elgg_models.river, rows)

    meta['progress'] = 40
    self.update_state(state='PROGRESS', meta=meta)

    # system_log
    rows = source_site.bulk_read(elgg_models.system_log)
    destination_site.bulk_write(elgg_models.system_log, rows)

    # users_entity
    rows = source_site.bulk_read(elgg_models.users_entity_with_pleio_guid)
    destination_site.bulk_write(elgg_models.users_entity_with_pleio_guid, rows)

    # config
    rows = source_site.bulk_read(elgg_models.config)
    destination_site.bulk_write(elgg_models.config, rows)

    # datalists
    rows = source_site.bulk_read(elgg_models.datalists)
    destination_site.bulk_write(elgg_models.datalists, rows)

    # some additional tasks from legacy to new
    if isinstance(source_backend, BackendWrapper):
        destination_site.initialize_export()
        destination_site.cleanup_plugins()
        destination_site.activate_pleio_plugin()

    destination_site.activate_site()

    meta['progress'] = 50
    self.update_state(state='PROGRESS', meta=meta)

    # copy files
    number_of_workers = 5
    total_files = 0
    files_copied = 0

    def worker():
        while True:
            file = q.get()
            if file is None:
                break

            source = os.path.join(source_site.data_root, file)
            target = os.path.join(destination_site.data_root, file)
            dirname = os.path.dirname(target)

            if not os.path.exists(dirname):
                try:
                    os.makedirs(dirname)
                except FileExistsError:
                    pass

            if os.path.exists(source):
                try:
                    copyfile(source, target)
                except IsADirectoryError:
                    print("Could not copy ", source, " to ", target, " because it is a directory, not a file.")
                except:
                    print("Catched uncaught exception when copying ", source, " to ", target)
            else:
                print("Could not find ", source)

            q.task_done()

    q = queue.Queue()
    threads = []
    for i in range(number_of_workers):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)

    for file in source_site.get_files():
        total_files += 1
        q.put(file)

    q.join()
    for i in range(number_of_workers):
        q.put(None)
    for t in threads:
        t.join()

    try:
        data_owner = settings.DATA_OWNER
    except AttributeError:
        data_owner = "www-data:www-data"

    subprocess.call(["chown", "-R", data_owner, destination_site.data_root])

    meta['progress'] = 100
    return meta

@shared_task(bind=True)
def delete_site(self, backend_key, site_id, site_host):
    delete_site.update_state(state='PROGRESS', meta={
        'type': 'delete_site'
    })

    backend = Manager.Instance().backends[backend_key]
    backend.delete_site(site_id)

    return {
        'type': 'delete_site',
        'backend_key': backend_key,
        'site_id': site_host
    }

@shared_task(bind=True)
def diskspace(self, backend_key, site_id):

    backend = Manager.Instance().backends[backend_key]

    try:
        usage = backend.get_site_file_size(site_id)
        free = backend.get_diskspace()
    except:
        usage = None
        free = None

    return {
        'usage': usage,
        'free': free
    }
