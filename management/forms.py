from django import forms
from django.core.exceptions import ValidationError


class CopyForm(forms.Form):
    def __init__(self, context, *args, **kwargs):
        super(CopyForm, self).__init__(*args, **kwargs)
        self.context = context
        self.fields['destination'].choices = [ (v.key, v.name) for v in context['backends'].values() if v.is_writable ]

        if context.get('overwrite'):
            self.fields['overwrite'] = forms.BooleanField(label='A site with this key already exists. Are you sure you want to overwrite the site?')

    destination = forms.ChoiceField(label='Bestemming', choices=[], widget=forms.Select(attrs={'class' : 'form-control'}))
    key = forms.CharField(label='Sleutel', widget=forms.TextInput(attrs={'class' : 'form-control'}))
    host = forms.CharField(label='Hostnaam', widget=forms.TextInput(attrs={'class' : 'form-control'}))

    def clean_key(self):
        data = self.cleaned_data
        if (data.get('destination') == self.context['backend'].key) and (data.get('key') == self.context['site'].key):
            raise ValidationError("Cannot copy a site to itself.")
        return data.get('key')


class EditForm(forms.Form):
    CHOICES = (
        ('test', 'Test'),
        ('prod', 'Productie'),
    )

    def __init__(self, context, *args, **kwargs):
        super(EditForm, self).__init__(*args, **kwargs)

    key = forms.CharField(label='Sleutel', widget=forms.TextInput(attrs={'class' : 'form-control', 'readonly': True}))
    host = forms.CharField(label='Hostnaam', widget=forms.TextInput(attrs={'class' : 'form-control'}))
    enabled = forms.BooleanField(label='Actief', required=False, widget=forms.CheckboxInput(attrs={'class': 'form-control'}))
    env = forms.ChoiceField(label='Omgeving', choices=CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

class DeleteForm(forms.Form):
    def __init__(self, context, *args, **kwargs):
        super(DeleteForm, self).__init__(*args, **kwargs)
        self.context = context

    key = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))

    def clean_key(self):
        value = self.cleaned_data.get('key')
        if value != self.context['site'].key:
            raise ValidationError("Please fill in the key of the site.")
        return value

