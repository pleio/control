import pytz
import os
from sqlalchemy import select, and_, or_
from functools import lru_cache
from datetime import datetime
from management import elgg_models

timezone = pytz.timezone("Europe/Amsterdam")

class FileHandler:
    def __init__(self, site):
        self.site = site
        self.backend = site.backend

        self.connection = self.site.connection.execution_options(stream_results=False)

    def read_files(self):
        for file in self.get_icons():
            yield file
        for file in self.get_featured_icons():
            yield file
        for file in self.get_files():
            yield file
        for file in self.get_images():
            yield file
        for file in self.get_newsletters():
            yield file
        for file in self.get_static():
            yield file
        for file in self.get_template_selector():
            yield file

    def write_file(self, source_file):
        raise NotImplementedError("This function is not supported in this backend.")

    def get_icons(self):
        join = elgg_models.entities.join(elgg_models.metadata, elgg_models.entities.c.guid == elgg_models.metadata.c.entity_guid)
        query = select([elgg_models.entities]).select_from(join).where(and_(
            elgg_models.entities.c.site_guid == self.site.id,
            elgg_models.metadata.c.name_id == self.get_metastring_id('icontime')
        ))

        for entity in self.connection.execute(query):
            subtype = self.get_subtype(entity.subtype)
            # do not copy icons of file objects as this is handled seperately
            # do not copy icons of static objects as these differ from the rest
            if subtype in ['file', 'static']:
                continue

            owner = self.get_entity(entity.owner_guid)
            if not owner:
                continue

            time_created = datetime.fromtimestamp(owner.time_created, timezone).strftime('%Y/%m/%d')

            path = None

            if entity.type == 'group':
                path = os.path.join(time_created, str(owner.guid), 'groups', str(entity.guid))
                sizes = ['tiny', 'small', 'medium', 'large', 'master', 'topbar']
            elif entity.type == 'object':
                if subtype == 'event':
                    path = os.path.join(time_created, str(owner.guid), 'events', str(entity.guid), '')
                    sizes = ['tiny', 'small', 'medium', 'large', 'master', 'topbar']
                elif subtype == 'blog':
                    path = os.path.join(time_created, str(owner.guid), 'blogs', str(entity.guid))
                    sizes = ['tiny', 'small', 'medium', 'large', 'master', 'topbar']

            if path:
                for size in sizes:
                    yield os.path.join(path + size + '.jpg')

    def get_featured_icons(self):
        join = elgg_models.entities.join(elgg_models.metadata, elgg_models.entities.c.guid == elgg_models.metadata.c.entity_guid)
        query = select([elgg_models.entities]).select_from(join).where(and_(
            elgg_models.entities.c.site_guid == self.site.id,
            elgg_models.metadata.c.name_id == self.get_metastring_id('featuredIcontime')
        ))

        for entity in self.connection.execute(query):
            subtype = self.get_subtype(entity.subtype)
            # do not copy icons of file objects as this is handled seperately
            # do not copy icons of static objects as these differ from the rest
            if subtype in ['file', 'static']:
                continue

            time_created = datetime.fromtimestamp(entity.time_created, timezone).strftime('%Y/%m/%d')

            path = os.path.join(time_created, str(entity.guid), 'featured', str(entity.guid))

            yield os.path.join(path + '.jpg')


    def get_files(self):
        for file in self.get_entities("object", "file"):
            file_metadata = self.get_metadata(file.guid)
            metadata_keys = set(file_metadata.keys())

            owner = self.get_entity(file.owner_guid)
            if not owner:
                continue

            time_created = datetime.fromtimestamp(owner.time_created, timezone).strftime('%Y/%m/%d')

            for key in metadata_keys.intersection(["filename", "thumbnail", "smallthumb", "largethumb"]):
                yield os.path.join(time_created, str(owner.guid), file_metadata[key])

    def get_images(self):
        for image in self.get_entities("object", "image"):
            image_metadata = self.get_metadata(image.guid)
            metadata_keys = set(image_metadata.keys())

            owner = self.get_entity(image.owner_guid)
            if not owner:
                continue

            time_created = datetime.fromtimestamp(owner.time_created, timezone).strftime('%Y/%m/%d')

            for key in metadata_keys.intersection(["filename", "thumbnail", "smallthumb", "largethumb"]):
                yield os.path.join(time_created, str(owner.guid), image_metadata[key])

    def get_newsletters(self):
        for newsletter in self.get_entities("object", "newsletter"):
            time_created = datetime.fromtimestamp(newsletter.time_created, timezone).strftime('%Y/%m/%d')

            for key in ['logging.json', 'recipients.json']:
                yield os.path.join(time_created, str(newsletter.guid), key)

    def get_static(self):
        for static in self.get_entities("object", "static"):
            time_created = datetime.fromtimestamp(static.time_created, timezone).strftime('%Y/%m/%d')

            for size in ['thumbtiny', 'thumbsmall', 'thumbmedium', 'thumblarge', 'thumbmaster', 'thumbtopbar']:
                yield os.path.join(time_created, str(static.guid), size + '.jpg')

    def get_template_selector(self):
        return [
            os.path.join('pleio_template_selector', 'favicon', 'favicon_' + str(self.site.id)),
            os.path.join('pleio_template_selector', 'site_logos', 'background_' + str(self.site.id)),
            os.path.join('pleio_template_selector', 'site_logos', 'logo_' + str(self.site.id))
        ]

    @lru_cache(maxsize=128)
    def get_metastring_id(self, metastring):
        row = self.connection.execute(select([elgg_models.metastrings]).where(
            elgg_models.metastrings.c.string == metastring
        )).fetchone()

        if row:
            return row.id
        return None

    @lru_cache(maxsize=128)
    def get_subtype_id(self, subtype):
        row = self.connection.execute(select([elgg_models.entity_subtypes]).where(and_(
            elgg_models.entity_subtypes.c.type == "object",
            elgg_models.entity_subtypes.c.subtype == subtype
        ))).fetchone()

        if row:
            return row.id
        return None

    @lru_cache(maxsize=128)
    def get_subtype(self, subtype_id):
        row = self.connection.execute(select([elgg_models.entity_subtypes]).where(and_(
            elgg_models.entity_subtypes.c.id == subtype_id
        ))).fetchone()

        if row:
            return row.subtype
        return None

    def get_entities(self, entity_type, subtype):
        query = select([elgg_models.entities]).where(and_(
            elgg_models.entities.c.type == entity_type,
            elgg_models.entities.c.subtype == (self.get_subtype_id(subtype) if subtype else 0),
            elgg_models.entities.c.site_guid == self.site.id
        ))

        return self.connection.execute(query)

    def get_entity(self, guid):
        row = self.connection.execute(select([elgg_models.entities]).where(
            elgg_models.entities.c.guid == guid
        )).fetchone()

        if row:
            return row
        return None

    def get_metadata(self, guid):
        msn = elgg_models.metastrings.alias()
        msv = elgg_models.metastrings.alias()

        join = elgg_models.metadata.join(msn, elgg_models.metadata.c.name_id == msn.c.id).join(msv, elgg_models.metadata.c.value_id == msv.c.id)
        rows = self.connection.execute(select([msn.c.string, msv.c.string]).select_from(join).where(
            elgg_models.metadata.c.entity_guid == guid
        )).fetchall()

        return {row[0]: row[1] for row in rows}
