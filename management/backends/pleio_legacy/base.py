from urllib.parse import urlparse
from sqlalchemy import create_engine, select, and_, or_
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.crypto import get_random_string
from management import elgg_models
from management.lib import iterate_in_batch
from .file import FileHandler

class BackendWrapper:
    type_name = 'pleio_legacy'
    is_writable = False

    def __init__(self, backend_key, backend_settings):
        self.key = backend_key
        self.name = backend_settings['NAME']
        self.data_root = backend_settings['DATA_ROOT']

        self.engine = create_engine(backend_settings['DATABASE'], pool_recycle=3600)
        self.connection = self.engine.connect()

    def __del__(self):
        if self.connection:
            self.connection.close()

    def get_sites(self):
        for site in self.connection.execute("SELECT * FROM elgg_sites_entity"):
            yield { 'id': site.guid, 'key': '-', 'host': urlparse(site.url).netloc, 'enabled': True, 'env': 'prod' }

    def get_site(self, site_id):
        site = self.connection.execute("SELECT * FROM elgg_sites_entity WHERE guid = %s", [site_id]).fetchone()

        if not site:
            raise ObjectDoesNotExist("Could not find site.")

        data = { 'id': site.guid, 'key': '-', 'host': urlparse(site.url).netloc, 'enabled': True, 'env': 'prod' }

        return BackendSite(self, data)

    def create_site(self, site_key):
        raise NotImplementedError("This function is not supported in this backend.")

    def edit_site(self, site_id, values):
        raise NotImplementedError("This function is not supported in this backend.")

    def delete_site(self, site_id):
        raise NotImplementedError("This function is not supported in this backend.")

    def key_exists(self, key):
        raise NotImplementedError("This function is not supported in this backend.")

class BackendSite:
    has_pleio_guid = False

    def __init__(self, backend, data):
        self.backend = backend
        self.connection = backend.engine.connect().execution_options(stream_results=True)

        self.id = data['id']
        self.key = data['key']
        self.host = data['host']
        self.enabled = data['enabled']
        self.env = data['env']

        self.content_owners = set()
        self.members = set()
        self.admins = set()

        self.relationships = set()
        self.acls = set()
        self.guids = set()
        self.msids = set()
        self.subsite_acl = 0

        self.filehandler = FileHandler(self)
        self.data_root = backend.data_root

    def __del__(self):
        self.connection.close()

    def get_values(self):
        return {k: getattr(self, k) for k in ['id', 'key', 'host', 'enabled', 'env']}

    def bulk_read(self, table):
        if table == elgg_models.access_collections:
            return self.get_access_collections()
        elif table == elgg_models.access_collection_membership:
            return self.get_access_collection_membership()
        elif table == elgg_models.annotations:
            return self.get_annotations()
        elif table == elgg_models.config:
            return self.get_config()
        elif table == elgg_models.datalists:
            return self.get_datalists()
        elif table == elgg_models.entities:
            return self.get_entities()
        elif table == elgg_models.entity_relationships:
            return self.get_entity_relationships()
        elif table == elgg_models.entity_subtypes:
            return self.get_entity_subtypes()
        elif table == elgg_models.objects_entity:
            return self.get_objects_entity()
        elif table == elgg_models.groups_entity:
            return self.get_groups_entity()
        elif table == elgg_models.sites_entity:
            return self.get_sites_entity()
        elif table == elgg_models.entity_views:
            return self.get_entity_views()
        elif table == elgg_models.entity_views_log:
            return self.get_entity_views_log()
        elif table == elgg_models.metadata:
            return self.get_metadata()
        elif table == elgg_models.metastrings:
            return self.get_metastrings()
        elif table == elgg_models.private_settings:
            return self.get_private_settings()
        elif table == elgg_models.river:
            return self.get_river()
        elif table == elgg_models.system_log:
            return self.get_system_log()
        elif table == elgg_models.users_entity_with_pleio_guid:
            return self.get_users_entity()
        else:
            return self.connection.execute(select([table]))

    def bulk_write(self, table, rows, ignore=True):
        raise NotImplementedError("This function is not supported in this backend.")

    def initialize_export(self):
        # get subsite access_id
        query = select([elgg_models.private_settings]).where(and_(
            elgg_models.private_settings.c.entity_guid == self.id,
            elgg_models.private_settings.c.name == "subsite_acl"
        ))
        row = self.connection.execute(query).fetchone()

        if row:
            self.subsite_acl = int(row.value)
        else:
            raise ValidationError("Could not find subsite access_id.")

        # get content owners
        join = elgg_models.annotations.join(elgg_models.entities, elgg_models.annotations.c.entity_guid == elgg_models.entities.c.guid)
        query = select([elgg_models.annotations]).select_from(join).where(elgg_models.entities.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.content_owners.add(row.owner_guid)

        query = select([elgg_models.entities]).where(elgg_models.entities.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.content_owners.add(row.owner_guid)

        query = select([elgg_models.metadata]).where(elgg_models.metadata.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.content_owners.add(row.owner_guid)

        query = select([elgg_models.river]).where(elgg_models.river.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.content_owners.add(row.subject_guid)

        # get members
        query = select([elgg_models.entity_relationships]).where(and_(
            elgg_models.entity_relationships.c.relationship == "member_of_site",
            elgg_models.entity_relationships.c.guid_two == self.id
        ))

        for row in self.connection.execute(query):
            self.members.add(row.guid_one)

        # get admins
        query = select([elgg_models.private_settings]).where(and_(
            elgg_models.private_settings.c.entity_guid == self.id,
            elgg_models.private_settings.c.name == "admin_guids"
        ))

        row = self.connection.execute(query).fetchone()
        if row:
            self.admins = set([int(x) for x in row.value.split(",")])

    def get_access_collections(self):
        query = select([elgg_models.access_collections]).where(elgg_models.access_collections.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.acls.add(row.id)
            yield row

    def get_access_collection_membership(self):
        for some_acls in iterate_in_batch(self.acls):
            query = select([elgg_models.access_collection_membership]).where(
                elgg_models.access_collection_membership.c.access_collection_id.in_(some_acls)
            )

            for row in self.connection.execute(query):
                yield row

    def get_annotations(self):
        join = elgg_models.annotations.join(elgg_models.entities, elgg_models.annotations.c.entity_guid == elgg_models.entities.c.guid)
        query = select([elgg_models.annotations]).select_from(join).where(elgg_models.entities.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.msids.add(row.name_id)
            self.msids.add(row.value_id)

            if row.access_id == self.subsite_acl:
                row = dict(row)
                row['access_id'] = 1

            yield row

    def get_config(self):
        return [
            {"name": "view", "value": "s:7:\"default\";", "site_guid": self.id},
            {"name": "language", "value": "s:2:\"nl\";", "site_guid": self.id},
            {"name": "default_access", "value": "s:1:\"1\";", "site_guid": self.id},
            {"name": "allow_registration", "value": "b:0;", "site_guid": self.id},
            {"name": "walled_garden", "value": "b:1;", "site_guid": self.id},
            {"name": "allow_user_default_access", "value": "i:0;", "site_guid": self.id},
        ]

    def get_datalists(self):
        return [
            {"name": "default_site", "value": self.id},
            {"name": "__site_secret__", "value": 'z' + get_random_string(31)}
        ]

    def get_entities(self):
        query = select([elgg_models.entities]).where(and_(
            or_(elgg_models.entities.c.site_guid == self.id, elgg_models.entities.c.guid == self.id),
            elgg_models.entities.c.type != "user"
        ))

        for row in self.connection.execute(query):
            self.guids.add(row.guid)

            row = dict(row)

            if row['type'] == 'site':
                row['subtype'] = 0
                row['owner_guid'] = 0
                row['site_guid'] = self.id
                row['container_guid'] = 0

            if row['access_id'] == self.subsite_acl:
                row['access_id'] = 1

            yield row

        users = self.content_owners.union(self.members)
        for some_users in iterate_in_batch(users):
            query = select([elgg_models.entities]).where(elgg_models.entities.c.guid.in_(some_users))
            for row in self.connection.execute(query):
                row = dict(row)
                row['site_guid'] = self.id

                if row['guid'] not in self.guids:
                    yield row

    def get_entity_relationships(self):
        for some_guids in iterate_in_batch(self.guids):
            query = select([elgg_models.entity_relationships]).where(elgg_models.entity_relationships.c.guid_one.in_(some_guids))
            for row in self.connection.execute(query):
                if row.id not in self.relationships:
                    self.relationships.add(row.id)
                    yield row

            query = select([elgg_models.entity_relationships]).where(elgg_models.entity_relationships.c.guid_two.in_(some_guids))
            for row in self.connection.execute(query):
                if row.id not in self.relationships:
                    self.relationships.add(row.id)
                    yield row

        for some_guids in iterate_in_batch(self.members):
            query = select([elgg_models.entity_relationships]).where(and_(
                elgg_models.entity_relationships.c.guid_one.in_(some_guids),
                elgg_models.entity_relationships.c.relationship.in_(['friendrequest', 'friend'])
            ))

            for row in self.connection.execute(query):
                if row.guid_two in self.members:
                    yield row

    def get_entity_subtypes(self):
        query = select([elgg_models.entity_subtypes])
        for row in self.connection.execute(query):
            row = dict(row)

            if row['type'] == 'site' and row['subtype'] == 'subsite':
                continue
            if row['type'] == 'object' and row['subtype'] == 'plugin':
                row['class'] = 'ElggPlugin'

            yield row

    def get_objects_entity(self):
        join = elgg_models.objects_entity.join(elgg_models.entities, elgg_models.objects_entity.c.guid == elgg_models.entities.c.guid)
        query = select([elgg_models.objects_entity]).select_from(join).where(elgg_models.entities.c.site_guid == self.id)
        return self.connection.execute(query)

    def get_groups_entity(self):
        join = elgg_models.groups_entity.join(elgg_models.entities, elgg_models.groups_entity.c.guid == elgg_models.entities.c.guid)
        query = select([elgg_models.groups_entity]).select_from(join).where(elgg_models.entities.c.site_guid == self.id)
        return self.connection.execute(query)

    def get_sites_entity(self):
        query = select([elgg_models.sites_entity]).where(elgg_models.sites_entity.c.guid == self.id)
        return self.connection.execute(query)

    def get_entity_views(self):
        query = select([elgg_models.entity_views]).where(elgg_models.entity_views.c.site_guid == self.id)
        return self.connection.execute(query)

    def get_entity_views_log(self):
        query = select([elgg_models.entity_views_log]).where(elgg_models.entity_views_log.c.site_guid == self.id)
        return self.connection.execute(query)

    def get_metadata(self):
        query = select([elgg_models.metadata]).where(elgg_models.metadata.c.site_guid == self.id)
        for row in self.connection.execute(query):
            self.msids.add(row.name_id)
            self.msids.add(row.value_id)

            if row.access_id == self.subsite_acl:
                row = dict(row)
                row['access_id'] = 1

            yield row

    def get_metastrings(self):
        for some_msids in iterate_in_batch(self.msids):
            query = select([elgg_models.metastrings]).where(elgg_models.metastrings.c.id.in_(some_msids))
            for row in self.connection.execute(query):
                yield row

    def get_private_settings(self):
        join = elgg_models.private_settings.join(elgg_models.entities, elgg_models.private_settings.c.entity_guid == elgg_models.entities.c.guid)
        query = select([elgg_models.private_settings]).select_from(join).where(elgg_models.entities.c.site_guid == self.id)
        for row in self.connection.execute(query):
            if row.name in ["path", "subsite_acl", "subsite_plugins_mandatory", "subsite_plugins_creation_available", "fallback_plugin_guid"]:
                continue
            yield row

    def get_river(self):
        query = select([elgg_models.river]).where(elgg_models.river.c.site_guid == self.id)
        for row in self.connection.execute(query):
            row = dict(row)

            if row['object_guid'] == 1:
                row['object_guid'] = self.id

            if row['access_id'] == self.subsite_acl:
                row['access_id'] = 1

            yield row

    def get_system_log(self):
        query = select([elgg_models.system_log]).where(elgg_models.system_log.c.site_guid == self.id)
        return self.connection.execute(query)

    def get_users_entity(self):
        users = self.content_owners.union(self.members)

        for some_users in iterate_in_batch(users):
            query = select([elgg_models.users_entity_without_pleio_guid]).where(elgg_models.users_entity_without_pleio_guid.c.guid.in_(some_users))
            for row in self.connection.execute(query):
                row = dict(row)
                row['code'] = ''
                row['password'] = ''
                row['salt'] = ''
                row['password_hash'] = ''

                # Copy the guid to pleio_guid as we would like to use this as linking attribute
                row['pleio_guid'] = row['guid']

                if row['guid'] not in self.members:
                    row['banned'] = 'yes'
                else:
                    row['banned'] = 'no'

                if row['guid'] in self.admins:
                    row['admin'] = 'yes'
                else:
                    row['admin'] = 'no'

                yield row

    def get_files(self):
        return self.filehandler.read_files()
