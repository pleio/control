import os
from sqlalchemy import create_engine, pool
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.crypto import get_random_string
from sqlalchemy import func
from sqlalchemy.sql import select, update, and_, or_
from management.lib import quote_identifier
from management import elgg_models
from .file import FileHandler
from time import time, mktime
from datetime import datetime, timedelta
from pathlib import Path
import shutil
import sqlparse
import math

class BackendWrapper:
    type_name = 'pleio'
    is_writable = True

    def __init__(self, backend_key, backend_settings):
        self.key = backend_key
        self.name = backend_settings['NAME']
        self.on_https = backend_settings['SITES_ON_HTTPS']
        self.data_root = backend_settings['DATA_ROOT']
        self.database_settings = backend_settings['DATABASE']

        self.engine = create_engine(self.database_settings, pool_pre_ping=True)
        self.instance_db = self.engine.url.database

    def get_connection(self):
        connection = self.engine.connect()
        return connection

    def get_sites(self):
        sites = []
        connection = self.get_connection()
        for site in connection.execute("SELECT * FROM instances"):
            sites.append({'id': site.id, 'key': site.username, 'host': site.host, 'enabled': (site.enabled == 1), 'env': site.env})
        connection.close
        return sites

    def get_site(self, site_id):
        connection = self.get_connection()
        site = connection.execute("SELECT * FROM instances WHERE id = %s", [site_id]).fetchone()
        connection.close()

        if not site:
            raise ObjectDoesNotExist("Could not find site.")

        data = {'id': site.id, 'key': site.username, 'host': site.host, 'enabled': (site.enabled == 1), 'env': site.env}

        return BackendSite(self, data, create=False)

    def get_site_db_size(self, site_id):
        site = self.get_site(site_id)

        connection = self.get_connection()
        db = connection.execute("SELECT table_schema AS k, SUM(data_length + index_length) / 1024 / 1024 AS s FROM information_schema.TABLES WHERE table_schema  = %s", [site.key]).fetchone()
        connection.close()
        if not db:
            return 0
            
        return db.s

    def get_site_file_size(self, site_id):
        site = self.get_site(site_id)

        root_directory = Path(os.path.join(self.data_root, site.key))
        usage_bytes = sum(f.stat().st_size for f in root_directory.glob('**/*') if f.is_file())
        usage_megabytes = round(usage_bytes / math.pow(1024, 2), 2)
        return usage_megabytes

    def get_diskspace(self):
        stats = shutil.disk_usage(self.data_root)
        free_gb = round(stats.free / math.pow(1024, 3), 2)
        return free_gb

    def create_site(self, values):
        connection = self.get_connection()
        row = connection.execute("SELECT id FROM instances WHERE username = %s", [values['key']]).fetchone()
        if not row:
            password = get_random_string(24)

            sql = "CREATE USER {0}@'%%' IDENTIFIED BY '{1}'".format(quote_identifier(values['key']), password)
            result = connection.execute(sql)

            sql = "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, EVENT, TRIGGER ON {0}.* TO {0}@'%%'".format(quote_identifier(values['key']))
            connection.execute(sql)

            connection.execute("FLUSH PRIVILEGES")

            connection.execute("INSERT INTO instances (host, name, username, password, enabled, env) VALUES (%s, %s, %s, %s, %s, %s)", [values['host'], values['key'], values['key'], password, values['enabled'], values['env']])
        connection.close()

        return BackendSite(self, values, create=True)

    def edit_site(self, site_id, values):
        connection = self.get_connection()
        connection.execute("UPDATE instances SET host = %s, enabled = %s, env = %s WHERE id = %s", [values['host'], values['enabled'], values['env'], site_id])
        connection.close()
        site = self.get_site(site_id)
        site.activate_site()

    def delete_site(self, site_id):
        site = self.get_site(site_id)

        if not site.key:
            raise ObjectDoesNotExist("Could not find the corresponding site key.")

        connection = self.get_connection()
        sql = "DROP DATABASE IF EXISTS {0}".format(quote_identifier(site.key))
        connection.execute(sql)

        connection.execute("DELETE FROM instances WHERE name = %s", [site.key])

        # grant usage so we do not get errors when user does not exist for some reason
        connection.execute("GRANT USAGE ON *.* TO {0}@`%%`".format(quote_identifier(site.key)))
        connection.execute("DROP USER {0}@`%%`".format(quote_identifier(site.key)))
        connection.execute("FLUSH PRIVILEGES")
        connection.close()

        shutil.rmtree(os.path.join(self.data_root, site.key))

    def key_exists(self, site_key):
        connection = self.get_connection()
        result = connection.execute("SHOW DATABASES LIKE %s", [site_key]).fetchone()
        connection.close()
        if result:
            return True
        return False

    def host_exists(self, site_host):
        connection = self.get_connection()
        result = connection.execute("SELECT id FROM instances WHERE host = %s", [site_host]).fetchone()
        connection.close()
        if result:
            return True

        return False

class BackendSite:
    has_pleio_guid = True

    def __init__(self, backend, data, create=False):
        self.backend = backend
        
        self.engine = create_engine(self.backend.database_settings, pool_pre_ping=True)
        self.connection = self.engine.connect().execution_options(
            isolation_level="READ UNCOMMITTED"
        )

        self.id = data['id']
        self.key = data['key']
        self.host = data['host']
        self.enabled = data['enabled']
        self.env = data['env']

        self.data_root = os.path.join(self.backend.data_root, data['key'])
        self.filehandler = FileHandler(self)

        if create:
            self.create()
        else:
            self.connect()

    def __del__(self):
        if self.connection:
            self.connection.close()
        self.engine.dispose()

    def get_values(self):
        return {k: getattr(self, k) for k in ['id', 'key', 'host', 'enabled', 'env']}

    def create(self):
        self.connection.execute("DROP DATABASE IF EXISTS %s" % quote_identifier(self.key))
        self.connection.execute("CREATE DATABASE %s" % quote_identifier(self.key))
        self.connection.execute("USE %s" % quote_identifier(self.key))

        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, 'database.sql'), 'r') as file:
            for query in sqlparse.split(file.read()):
                self.connection.execute(query)

    def connect(self):
        self.connection.execute("USE %s" % quote_identifier(self.key))

    def initialize_export(self):
        query = select([elgg_models.datalists]).where(elgg_models.datalists.c.name == 'default_site')
        row = self.connection.execute(query).fetchone()

        if not row:
            raise ValidationError("Could not find default site id")

        self.id = int(row.value)

    def bulk_read(self, table):
        if table == elgg_models.datalists:
            return self.get_datalists()
        else:
            return self.connection.execution_options(stream_results=True).execute(select([table]))

    def get_datalists(self):
        return [
            {"name": "default_site", "value": self.id},
            {"name": "__site_secret__", "value": 'z' + get_random_string(31)}
        ]

    def bulk_write(self, table, rows):
        batch = []
        i = 0

        for row in rows:
            batch.append(row)
            i += 1

            if i == 5000:
                self.connection.execute(table.insert(), batch)
                batch = []
                i = 0

        if i > 0:
            self.connection.execute(table.insert(), batch)

    def get_files(self):
        return self.filehandler.read_files()

    def cleanup_plugins(self):
        allowed_plugins = [
            'account_removal', 'addthis', 'advanced_comments', 'advanced_notifications', 'analytics', 'apiadmin', 'backup', 'birthdays', 'blog', 'blog_tools',
            'bookmarks', 'bookmarks_tools', 'categories', 'content_redirector', 'content_subscriptions', 'csv_exporter', 'custom_css', 'dashboard', 'digest', 'dutch_translation',
            'elasticsearch', 'embed', 'embedded_login', 'entity_tools', 'entity_view_counter', 'etherpad', 'event_manager', 'externalpages', 'file', 'file_tools',
            'fontawesome', 'french_translation', 'garbagecollector', 'group_tools', 'groups', 'html_email_handler', 'htmlawed', 'language_selector', 'lazy_hover', 'likes',
            'members', 'mentions', 'menu_builder', 'messageboard', 'news', 'newsletter', 'notifications', 'odt_editor', 'pages', 'pages_tools',
            'pinboard', 'pleio_beconnummer', 'pleio_login', 'pleio_rest', 'pleio_template', 'pleio_template_selector', 'pleio_test', 'pleiobox', 'pleiofile', 'plugin_manager',
            'polls', 'profile', 'profile_manager', 'questions', 'quicklinks', 'reportedcontent', 'rewrite', 'rijkshuisstijl', 'search', 'security_tools', 'spam_login_filter',
            'static', 'tagcloud', 'tasks', 'thewire', 'thewire_tools', 'tidypics', 'tinymce', 'tinymce_extended', 'todos', 'translation_editor',
            'uservalidationbyemail', 'videolist', 'videos', 'widget_manager', 'wizard', 'ws_pack'
        ]

        join = elgg_models.objects_entity.join(elgg_models.entities,
            elgg_models.objects_entity.c.guid == elgg_models.entities.c.guid
        )

        rows = self.connection.execute(select([elgg_models.objects_entity]).select_from(join).where(and_(
            elgg_models.entities.c.type == 'object',
            elgg_models.entities.c.subtype == 2,
            ~elgg_models.objects_entity.c.title.in_(allowed_plugins)
        )))

        guids = [row[0] for row in rows]

        for guid in guids:
            self.connection.execute(elgg_models.entities.delete(elgg_models.entities.c.guid == guid))
            self.connection.execute(elgg_models.objects_entity.delete(elgg_models.objects_entity.c.guid == guid))
            self.connection.execute(elgg_models.private_settings.delete(elgg_models.private_settings.c.entity_guid == guid))
            self.connection.execute(elgg_models.metadata.delete(elgg_models.metadata.c.entity_guid == guid))
            self.connection.execute(elgg_models.entity_relationships.delete(elgg_models.entity_relationships.c.guid_one == guid))
            self.connection.execute(elgg_models.entity_relationships.delete(elgg_models.entity_relationships.c.guid_two == guid))

    def activate_pleio_plugin(self):
        result = self.connection.execute(elgg_models.entities.insert({
            'type': 'object',
            'subtype': 2,
            'owner_guid': self.id,
            'site_guid': self.id,
            'container_guid': self.id,
            'access_id': 2,
            'time_created': time(),
            'time_updated': time(),
            'last_action': time()
        }))

        self.connection.execute(elgg_models.objects_entity.insert({
            'guid': result.lastrowid,
            'title': 'pleio',
            'description': ''
        }))

        self.connection.execute(elgg_models.private_settings.insert({
            'entity_guid': result.lastrowid,
            'name': 'elgg:internal:priority',
            'value': 'latest'
        }))

        self.connection.execute(elgg_models.entity_relationships.insert({
            'guid_one': result.lastrowid,
            'relationship': 'active_plugin',
            'guid_two': self.id,
            'time_created': time()
        }))

    def is_pleio_template_active(self):
        query = select([elgg_models.objects_entity.c.guid]).where(elgg_models.objects_entity.c.title == 'pleio_template')
        plugin_guid = self.connection.execute(query).scalar()
        
        if plugin_guid:
            query = select([func.count(elgg_models.entity_relationships.c.id)]).where(and_(elgg_models.entity_relationships.c.relationship == 'active_plugin', elgg_models.entity_relationships.c.guid_one == plugin_guid))
            return self.connection.execute(query).scalar() > 0

        return False


    def activate_site(self):
        prefix = 'https://' if self.backend.on_https else 'http://'
        url = prefix + self.host + '/'
        self.connection.execute(update(elgg_models.sites_entity).values(url=url))

    def get_user_count(self, is_active=None):
        date = datetime.today() - timedelta(days=30)
        is_active_date = int(mktime(date.timetuple()))

        query = select([func.count(elgg_models.users_entity_with_pleio_guid.c.guid)])
        
        if is_active == True:
            query = query.where(or_(elgg_models.users_entity_with_pleio_guid.c.last_login > is_active_date, elgg_models.users_entity_with_pleio_guid.c.last_action > is_active_date))
        elif is_active == False:
            query = query.where(or_(elgg_models.users_entity_with_pleio_guid.c.last_login <= is_active_date, elgg_models.users_entity_with_pleio_guid.c.last_action <= is_active_date))
        
        return self.connection.execute(query).scalar()


