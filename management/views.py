from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.http import Http404
from django.shortcuts import render, redirect
from django.conf import settings
from sqlalchemy.exc import OperationalError
from sqlalchemy.sql import select

from . import elgg_models
from .lib import Manager
from .forms import CopyForm, EditForm, DeleteForm
from .tasks import copy_site, delete_site, diskspace

def overview(request, backend_key=None):
    return render(request, 'overview.html', _get_context(backend_key))

def copy(request, backend_key, site_id):
    context = _get_context(backend_key, site_id)

    if not request.POST:
        diskusage_task = diskspace.delay(backend_key, site_id)

        form = CopyForm(context)
        context['form'] = form
        context['db_size'] = context['backend'].get_site_db_size(site_id)
        context['disk_usage_task'] = diskusage_task.id
        return render(request, 'copy.html', context)

    if request.POST.get('overwrite'):
        context['overwrite'] = True

    form = CopyForm(context, request.POST)

    if not form.is_valid():
        context['form'] = form
        return render(request, 'copy.html', context)

    data = form.cleaned_data

    try:
        destination_backend = context['backends'][data['destination']]
    except KeyError:
        raise Http404("Destination backend does not exist")

    if destination_backend.key_exists(data['key']) and not data.get('overwrite'):
        context['overwrite'] = True
        context['form'] = CopyForm(context, request.POST)
        return render(request, 'copy.html', context)

    copy_site.delay(backend_key, site_id, data['destination'], data['key'], data['host'])

    messages.add_message(request, messages.SUCCESS, 'The copy is scheduled successfully.')
    return redirect('management:overview', backend_key=backend_key)

def edit(request, backend_key, site_id):
    context = _get_context(backend_key, site_id)
    backend = context['backend']

    if request.POST:
        form = EditForm(context, request.POST)
        if form.is_valid():
            try:
                backend.edit_site(site_id, form.cleaned_data)
                messages.add_message(request, messages.SUCCESS, 'The site is edited successfully.')
            except NotImplementedError:
                messages.add_message(request, messages.ERROR, 'Could not edit site as the function is not implemented.')
            return redirect('management:overview', backend_key=backend_key)
    else:
        form = EditForm(context, context['site'].get_values())

    context['form'] = form
    return render(request, 'edit.html', context)

def delete(request, backend_key, site_id):
    context = _get_context(backend_key, site_id)
    backend = context['backend']

    if request.POST:
        form = DeleteForm(context, request.POST)
        if form.is_valid():
            delete_site.delay(backend_key, site_id, context['site'].host)
            messages.add_message(request, messages.SUCCESS, 'The deletion of the is scheduled successfully.')
            return redirect('management:overview', backend_key=backend_key)
    else:
        form = DeleteForm(context)

    context['form'] = form
    return render(request, 'delete.html', context)

def _get_context(backend_key=None, site_id=None):
    manager = Manager.Instance()

    context = {
        'backends': manager.backends
    }

    if backend_key:
        try:
            context['backend'] = manager.backends[backend_key]
        except KeyError:
            raise Http404("Backend does not exist")

    if site_id:
        try:
            context['site'] = manager.backends[backend_key].get_site(site_id)
        except Exception:
            raise Http404("Site does not exist")

    return context


def admin_email_addresses(request, backend_key):
    admin_email_addresses = []
    manager = Manager.Instance()

    source_backend = manager.backends[backend_key]
    sites = source_backend.get_sites()

    for site in sites:
        try:
            source_site = source_backend.get_site(site['id'])
        except Exception:
            continue

        if source_site.has_pleio_guid:
            query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.admin == 'yes')
            rows = source_site.connection.execute(query)
        else:
            query = select([elgg_models.users_entity_without_pleio_guid]).where(elgg_models.users_entity_without_pleio_guid.c.admin == 'yes')
            rows = source_site.connection.execute(query)

        admin_email_addresses = admin_email_addresses + [user['email'] for user in rows]

    return render(request, 'admin_email_addresses.html', {'admin_email_addresses': sorted(list(set(admin_email_addresses)))})

