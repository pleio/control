from django.core.exceptions import ImproperlyConfigured
from importlib import import_module
from django.conf import settings


class Singleton:

    def __init__(self, cls):
        self._cls = cls

    def Instance(self):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._cls()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._cls)

@Singleton
class Manager:

    def __init__(self):
        self.backends = {k:self.initialize_backend(k, v) for k,v in settings.TRANSFER_BACKENDS.items()}

    def initialize_backend(self, backend_key, backend_settings):
        try:
            backend = import_module('%s.base' % backend_settings['BACKEND'])
            return backend.BackendWrapper(backend_key, backend_settings)
        except ImportError as e_user:
            raise ImproperlyConfigured(
                "%s is not a valid management backend.\n"
                "Please check management.backends.XXX and very the name." % (backend_settings['BACKEND'])
            ) from e_user

    def __str__(self):
        return 'Manager object'

def quote_identifier(identifier, for_grants=False):
    if for_grants:
        return '`' + identifier.replace('`', '``').replace('_', r'\_') \
            .replace('%', r'\%%') + '`'
    else:
        return '`' + identifier.replace('`', '``').replace('%', '%%') + '`'

def shellquote(s):
    return "'" + s.replace("'", "'\\''") + "'"

def iterate_in_batch(iterator, n=100):
    items = set()

    for item in iterator:
        items.add(item)

        if len(items) == n:
            yield items
            items = set()

    if len(items) > 0:
        yield items