import requests
from django.conf import settings


def get_sites_by_organisation(pages):
    sites = {}
    if hasattr(settings, 'MONEYBIRD_ADMINISTRATION_ID') and hasattr(settings, 'MONEYBIRD_API_TOKEN'):
        for page in range(1, pages):
            url = 'https://moneybird.com/api/v2/' + settings.MONEYBIRD_ADMINISTRATION_ID + '/contacts?page=' + str(page) + '&per_page=100'
            headers = {'Authorization': 'Bearer ' + settings.MONEYBIRD_API_TOKEN}
            r = requests.get(url, headers=headers)
            
            data = r.json()
            if data and not 'error' in data:
                for contact in r.json():
                    name = ""
                    organisation = {}
                    for cf in contact['custom_fields']:
                        if cf['name'] == 'Naam deelsite':
                            name = cf['value']
                            organisation['organisation'] = ""
                            organisation['organisation_type'] = "" 
                            organisation['site_type'] = ""
                        if cf['name'] == 'Organisatie':
                            organisation['organisation'] = cf['value']
                        if cf['name'] == 'Type organisatie':
                            organisation['organisation_type'] = cf['value']
                        if cf['name'] == 'Type deelsite':
                            organisation['site_type'] = cf['value']
                    sites[name] = organisation
    return sites
