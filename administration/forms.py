from django import forms
from django.core.exceptions import ValidationError


class UserForm(forms.Form):
    guid_email = forms.CharField(label='guid/email', max_length=100)
