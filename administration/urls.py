from django.urls import path
from . import views

app_name = 'administration'

urlpatterns = [
    path('<str:backend_key>/sites/', views.get_sites, name='sites'),
    path('<str:backend_key>/users/', views.get_user, name='users'),
    path('<str:backend_key>/admins/', views.get_admins, name='admins'),
    path('<str:backend_key>/', views.overview, name='overview'),
    path('', views.overview, name='overview'),
]
