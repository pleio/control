import re
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.http import Http404
from django.shortcuts import render, redirect
from sqlalchemy.sql import and_, select
from management import elgg_models
from management.lib import Manager
from administration.utils import get_sites_by_organisation

from .forms import UserForm


def overview(request, backend_key=None):
    return render(request, 'administration/overview.html', _get_context(backend_key))


def get_user(request, backend_key):
    users = []
    if request.method == 'POST':
        form = UserForm(request.POST)

        if form.is_valid():
            try:
                guid = int((form.data['guid_email']).strip())
                email = ""
            except:
                email = form.data['guid_email'].strip()
                guid = 0
            if email:
                if not re.match(r"^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email):
                    messages.add_message(request, messages.ERROR, 'Geen geldige guid of email adres.')
                    return redirect('/administration/' + backend_key + '/users/')

            manager = Manager.Instance()

            source_backend = manager.backends[backend_key]
            sites = source_backend.get_sites()

            for site in sites:
                try:
                    source_site = source_backend.get_site(site['id'])
                except Exception:
                    continue

                if source_site.has_pleio_guid:
                    if guid:
                        query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.pleio_guid == guid)
                    else:
                        query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.email == email)
                    rows = source_site.connection.execute(query)
                else:
                    if guid:
                        query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.pleio_guid == guid)
                    else:
                        query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.email == email)
                    rows = source_site.connection.execute(query)

                for user in rows:

                    users.append({'email': user['email'], 'pleio_guid': user["pleio_guid"], 'site': site})
    else:
        form = UserForm()

    return render(request, 'administration/administration.html', {'form': form, 'users': users})


def get_sites(request, backend_key):
    users = []
    sites_data = []

    manager = Manager.Instance()

    source_backend = manager.backends[backend_key]
    sites = source_backend.get_sites()

    org_sites = get_sites_by_organisation(7)
    for site in sites:
        pleio_template = False
        organisation = ''
        organisation_type = ''
        site_type = ''
        try:
            source_site = source_backend.get_site(site['id'])
        except Exception:
            continue

        if site['key'] in org_sites.keys():
            organisation = org_sites[site['key']]['organisation']
            organisation_type = org_sites[site['key']]['organisation_type']
            site_type = org_sites[site['key']]['site_type']

        pleio_template = source_site.is_pleio_template_active()

        active_users = source_site.get_user_count(True)
        total_users = source_site.get_user_count()

        sites_data.append({'url': site['host'], 'site_type': site_type, 'organisation': organisation, 'organisation_type': organisation_type, 'total_users': total_users, "active_users": active_users,
                           'pleio_template': pleio_template, 'env': site['env']})

        del source_site

    return render(request, 'administration/sites.html', {'sites': sites_data})


def get_admins(request, backend_key):
    admins = []
    manager = Manager.Instance()

    source_backend = manager.backends[backend_key]
    sites = source_backend.get_sites()

    for site in sites:
        try:
            source_site = source_backend.get_site(site['id'])
        except Exception:
            continue

        pleio_template = source_site.is_pleio_template_active()
        
        if source_site.has_pleio_guid:
            query = select([elgg_models.users_entity_with_pleio_guid]).where(elgg_models.users_entity_with_pleio_guid.c.admin == 'yes')
            rows = source_site.connection.execute(query)
        else:
            query = select([elgg_models.users_entity_without_pleio_guid]).where(elgg_models.users_entity_without_pleio_guid.c.admin == 'yes')
            rows = source_site.connection.execute(query)

        for admin in rows:
            admins.append({'email': admin['email'], 'name': admin['name'], 'site': site['host'], 'pleio_template': pleio_template, 'env': site['env']})

        admins = list({v['email']:v for v in admins}.values())

    return render(request, 'administration/admins.html', {'admins': admins})


def _get_context(backend_key=None, site_id=None):
    manager = Manager.Instance()

    context = {
        'backends': manager.backends
    }

    if backend_key:
        try:
            context['backend'] = manager.backends[backend_key]
        except KeyError:
            raise Http404("Backend does not exist")

    if site_id:
        try:
            context['site'] = manager.backends[backend_key].get_site(site_id)
        except ObjectDoesNotExist:
            raise Http404("Site does not exist")

    return context