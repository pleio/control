# Stage 1 - Compile needed Python dependencies
FROM python:3.6-slim AS python-env

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    python3-dev \
    default-libmysqlclient-dev 

RUN python -m venv /app-env && /app-env/bin/pip install --upgrade pip

WORKDIR /app-env
COPY requirements.txt /app-env
RUN /app-env/bin/pip3 install -r requirements.txt

# Stage 2 - Build docker image suitable for execution and deployment
FROM python:3.6-slim
RUN apt-get update && apt-get install --no-install-recommends -y \
    ca-certificates \
    libmariadb3 \
    gettext \
    mime-support

COPY --from=python-env /app-env /app-env
ENV PATH="/app-env/bin:${PATH}"

COPY . /app
COPY ./docker/start.sh /start.sh
COPY ./docker/start-dev.sh /start-dev.sh
RUN chmod +x /start.sh /start-dev.sh

RUN mkdir -p /app/static /app/data && chown -R www-data:www-data /app/static /app/data

USER www-data

WORKDIR /app
EXPOSE 8000
CMD ["/start.sh"]
