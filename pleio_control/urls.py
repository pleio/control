from django.urls import path, include
from django.views.generic import RedirectView
from django.contrib import admin

from two_factor.urls import urlpatterns as tf_urls
from management.urls import urlpatterns as management_urls
from administration.urls import urlpatterns as administration_urls

import core.views as core_views
import management.views as management_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', core_views.home, name='home'),
    path('tasks/', core_views.tasks, name='tasks'),
    path('tasks/status', core_views.task_status, name='task_status'),
    path('account/logout/', core_views.logout, name='logout'),
    path('', RedirectView.as_view(url='home')),
    path('', include(tf_urls)),
    path('management/', include('management.urls')),
    path('administration/', include('administration.urls')),
]