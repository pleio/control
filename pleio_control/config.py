import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('ENABLE_DEBUG')

ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST')]

TRANSFER_BACKENDS = {
    os.getenv('TRANSFER_BACKENDS_NAME'): {
        'NAME': os.getenv('TRANSFER_BACKENDS_NAME'),
        'BACKEND': 'management.backends.pleio',
        'DATABASE': os.getenv('TRANSFER_BACKENDS_NAME_DATABASE'),
        'DATA_ROOT': os.getenv('TRANSFER_BACKENDS_NAME_DATAROOT'),
        'SITES_ON_HTTPS': os.getenv('TRANSFER_BACKENDS_SITES_ON_HTTPS')
    }
}

CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')

MONEYBIRD_ADMINISTRATION_ID = os.getenv('MONEYBIRD_ADMINISTRATION_ID')
MONEYBIRD_API_TOKEN = os.getenv('MONEYBIRD_API_TOKEN')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}
